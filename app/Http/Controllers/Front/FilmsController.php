<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Attachment;
use App\Models\Building;
use App\Models\ClientProject;
use App\Models\Film;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

use Illuminate\Http\Request;


class FilmsController extends Controller
{
    public function showList()
    {
        return view('films.list');
    }

    public function showFilmBySlug($slug)
    {
        $film = Film::findBySlugOrFail($slug);
        return view('films.details', compact('film'));
    }

    public function showCreateForm()
    {
        return view('films.create');
    }


}

