<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\Attachment;
use App\Models\Building;
use App\Models\ClientProject;
use App\Models\Film;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;


class FilmsController extends Controller
{
    public function getListOfFilms()
    {
        return Film::all();
    }

    public function getFilm(Film $film)
    {
        return $film;
    }

    public function createFilm()
    {
        $filmInput = json_decode(request()->all()['film'], true);
        $film = new Film($filmInput);

        $this->validGenre($film);

        $file = Input::file('photo');
        $fileContents = file_get_contents($file->getRealPath());

        $path =  'public/films/' . uniqid("file",true);
        Storage::put($path, $fileContents);

        //return $film;
        $film->photo_path = $path;
        $film->photo = $this->getFileUrl($path);
        $film->save();

        return $film;
    }

    private function validGenre($film) {
        if($film->genre == []) abort(500); // 500 because we are not hacker friendly
        foreach($film->genre as $genre) {
            if(!in_array($genre, Film::GENRE_LIST)) abort(500);
        }
    }

    public function getFileUrl($file_path) {
        if(Storage::getDriver()->getAdapter() instanceof LocalAdapter) {
            return env('APP_URL').Storage::url($file_path);
        } else {
            return Storage::url($file_path);
        }
    }

    public function getGenreList() {
        return Film::GENRE_LIST;
    }
}

