<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\Attachment;
use App\Models\Building;
use App\Models\ClientProject;
use App\Models\Comment;
use App\Models\Film;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;


class CommentsController extends Controller
{
    public function getListOfComments(Film $film)
    {
        return $film->comments()->orderBy('comments.id')->get();
    }

    public function createComment(Film $film)
    {
        $comment = new Comment(request()->all());
        $comment->film_id = $film->id;
        $comment->user_id = Auth::user()->id;
        $comment->save();
    }


}

