<?php

use App\Models\Film;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\Film::class, function (Faker $faker)
{
    $nbWords = $faker->numberBetween(1,5);
    $sentence = $faker->sentence($nbWords);
    $title = substr($sentence, 0, strlen($sentence) - 1);

    return [
        'name' => $title,
        'description' => $faker->text(200),
        'release_date' => $faker->dateTimeBetween('-30 years', 'now'),
        'rating' => $faker->numberBetween(1,5),
        'ticket_price' => $faker->randomFloat(2,10,100),
        'country' => $faker->country,
        'genre' => $faker->randomElements(Film::GENRE_LIST,2),
        //'photo' => 'http://lorempixel.com/200/300?'.Film::count(),
        'photo' => 'https://picsum.photos/200/300?image='. (500+$faker->numberBetween(1,100)),
    ];
});
