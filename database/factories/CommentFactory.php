<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\Comment::class, function (Faker $faker) {

    $user = App\Models\User::all()->random();
    $film = App\Models\Film::all()->random();

    return [
        'user_id' =>  $user->id,
        'film_id' =>  $film->id,
        'name' => $user->name,
        'comment' => $faker->text(200),
    ];
});
