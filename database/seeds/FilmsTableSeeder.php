<?php

use Illuminate\Database\Seeder;

class FilmsTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        factory(App\Models\Film::class)->make()->save();
        factory(App\Models\Film::class)->make()->save();
        factory(App\Models\Film::class)->make()->save();


    }
}
