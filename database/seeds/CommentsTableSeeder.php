<?php

use Illuminate\Database\Seeder;


class CommentsTableSeeder extends Seeder
{

    public function run()
    {
        $films = App\Models\Film::all();

        foreach($films as $film)
        {
            $comment = factory(App\Models\Comment::class)->make();
            $comment->film_id = $film->id;
            $comment->save();
        }

    }
}

