<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { return redirect()->route('films'); }); //return view('welcome');

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

Route::group([
    'namespace' => 'Front',
], function ()
{
    Route::get('/films', 'FilmsController@showList')->name('films');
    Route::get('/films/create', 'FilmsController@showCreateForm')->name('films.create');
    Route::get('/films/{slug}', 'FilmsController@showFilmBySlug')->name('films.slug');

});

// we move below two routes fro api.php for simplicity (to avoid api auth e.g. by passport OAuth)
Route::get('api/v1/users/is-authenticated', function() { return [ "auth" => !! \Auth::user()]; } );
Route::post('api/v1/films/{film}/comments', 'Api\V1\CommentsController@createComment');


