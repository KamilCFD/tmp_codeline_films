<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    'prefix' => 'v1',
    'namespace' => 'Api\V1',
    // 'middleware' => ['auth:api'],
], function ()
{
    Route::get('films', 'FilmsController@getListOfFilms');
    Route::get('films/genre-list', 'FilmsController@getGenreList');
    Route::get('films/{film}', 'FilmsController@getFilm');
    Route::post('films', 'FilmsController@createFilm');

    Route::get('films/{film}/comments', 'CommentsController@getListOfComments');


});