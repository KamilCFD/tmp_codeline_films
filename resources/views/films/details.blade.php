@extends('layouts.app')

@section('content')
    <film-details :film-id="{{ $film->id }}"></film-details>
@endsection