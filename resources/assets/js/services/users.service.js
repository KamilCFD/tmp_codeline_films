export class UsersService
{
    static api_url() {
        return '/api/v1/';
    }

    static async isAuthenticated()
    {
        const response = await axios.get(UsersService.api_url() + 'users/is-authenticated');
        return response.data;
    }

}