export class FilmsService
{
    static api_url() {
        return '/api/v1/films';
    }

    static async getListOfFilms()
    {
        const response = await axios.get(FilmsService.api_url());
        return response.data;
    }

    static async getFilm(filmId)
    {
        const response = await axios.get(FilmsService.api_url() + '/' + filmId);
        return response.data;
    }

    static async getGenreList()
    {
        const response = await axios.get(FilmsService.api_url() + '/genre-list');
        return response.data;
    }

    static async saveFilm(film, photo)
    {
        // const headers = {
        //     headers: {
        //         'Accept': 'application/json',
        //         'Content-Type': 'application/json',
        //         'Access-Control-Allow-Origin': '*',
        //     }
        // };
        //
        // const response = await axios.post(FilmsService.api_url(), { test: "aaa"}, headers);
        // return response.data;

        let formData = new FormData();
        formData.append("photo", photo);
        formData.append("film", JSON.stringify(film));

        const response = await axios.post(FilmsService.api_url(), formData, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        });
        return response.data;
    }


}