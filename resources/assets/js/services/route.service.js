export class RouteService
{
    static front_url() {
        return '/films';
    }

    static async redirectToListOfFilms() {
        window.location.href = RouteService.front_url()
    }

    static redirectToFilmDetails(slug) {
        window.location.href = RouteService.front_url() + '/' + slug;
    }

    static goToCreateFilm() {
        window.location.href = RouteService.front_url() + '/create' ;
    }
}