export class CommentsService
{
    static api_url() {
        return '/api/v1/';
    }

    static async getListOfComments(filmId)
    {
        //console.log('zz',filmId, CommentsService.api_url() + 'films/' + filmId);
        const response = await axios.get(CommentsService.api_url() + 'films/' + filmId + '/comments');
        return response.data;
    }

    static async saveComment(comment, filmId)
    {
        console.log('xaa∂x');
        // const headers = {
        //     headers: {
        //         'Accept': 'application/json',
        //         'Content-Type': 'application/json',
        //         'Access-Control-Allow-Origin': '*',
        //     }
        // };
        //
        // const response = await axios.post(FilmsService.api_url(), { test: "aaa"}, headers);
        // return response.data;

        // let formData = new FormData();
        // formData.append("photo", photo);
        // formData.append("film", JSON.stringify(film));

        const response = await axios.post(CommentsService.api_url() + 'films/' + filmId + '/comments', comment);
        return response.data;
    }


}