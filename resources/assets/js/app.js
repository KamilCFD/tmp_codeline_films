
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('loader', require('./components/tools/Loader.vue'));
Vue.component('film-list', require('./components/films/FilmList.vue'));
Vue.component('film-item', require('./components/films/FilmItem.vue'));
Vue.component('film-details', require('./components/films/FilmDetails.vue'));
Vue.component('film-create', require('./components/films/FilmCreate.vue'));
Vue.component('comment-list', require('./components/comments/CommentList.vue'));
Vue.component('comment-item', require('./components/comments/CommentItem.vue'));
Vue.component('comment-create', require('./components/comments/CommentCreate.vue'));


const app = new Vue({
    el: '#app'
});
