# To run project:

1. Copy .env.example to .env, edit it and set proper DB/Mail variables. 
2. `composer install`
3. `php artisan migrate --seed`
4. `php artisan storage:link`
5. `php artisan key:generate`
7. `composer dump-autoload`
6. `npm isntall`
7. `npm run watch`




